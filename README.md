
# = Introduction =

This is a small template maven project for Java and Kotlin. Some popular libraries are already included.

Why Kotlin AND Java?

* Kotlin is the superior language in some aspects.
* Java is very popular, and sometimes you might just want to copy some Java code from the internet or an existing 
 project into your Kotlin project. Also java has records built in, whereas in Kotlin, records require a workaround with data classes.

Note: This template project is mainly a copy-and-paste of many stackoverflow threads and online tutorials, blogs posts
as well as the official Kotlin, Maven and other documentations. Thanks to all the people writing this awesome stuff,
it helps a lot. :)

# = Usage =

## Maven

Run ``mvn install`` in the project's root directory to build the project.  
This runs some dummy tests and builds an executable hello-kotlin jar (you can change to a hello-java jar in the pom.xml). You can run the jar for example with:  
``java -jar target/template-0.0.1-SNAPSHOT-jar-with-dependencies.jar``

Or, you could execute the built dummy application via maven. For that, run:  
``mvn exec:java``

### Troubleshoot

#### On mvn install: "system modules path not set in conjunction with -source *XX*"

The system Java version (``java --version``) does not match the version configured in the ``pom.xml``. Probably you can
silence this warning somewhere. (Or remove the part from the pom.xml which makes the build fail on warnings.)

# = IDE Configuration =

## - IntelliJ IDEA -

### Setup

Open the project, select "Maven Project" in the open dialog. I'd also recommend to install the 'Maven Helper' plugin via "Settings" -> "Plugins".

#### Configure the SDK:

1) "File"
2) "Project Structure..."
3) "Project"
4) "Project SDK:"
5) Select the correct SDK which also is configured in the ``pom.xml``
6) Also adjust the "Project Language Level" accordingly

## - Eclipse -

Honestly, better forget about Eclipse at the moment. The Kotlin plugin seems to be broken, at least I could not get it to
run. Maybe you have more luck. Anyways, some setup and troubleshooting hints:

### Setup

####  Install the Kotlin plugin for eclipse:

Follow the tutorial at: https://www.kotlintutorialblog.com/run-kotlin-program-eclipse-ide/

#### Configure Kotlin nature for the project:

1) Rightclick on project
2) "Configure Kotlin"
3) "Add Kotlin Nature"

#### Configure the Kotlin source folders:

1) Rightclick on project
2) "Build Path"
3) "Configure Build Path..."
4) "Add Folder"
5) Select add the Kotlin main and test source folders
6) "Apply and Close"

#### Switch to Kotlin perspective:

1) "Window"
2) "Perspective"
3) "Open Perspective"
4) "Other..."
5) "Kotlin"

### Troubleshoot

#### "Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.5.1:compile (java-compile) on project template: Fatal error compiling: error: invalid target release: ..."

Install the mentioned Java version on your system.  
For example on linux: ``sudo apt install openjdk-17-jdk``


#### Strange propblems in eclipse?

Might be incompatibility of older eclipse version with newer Java versions. Install the current eclipse release.
    
#### "Implicit super constructor Object() is undefined for default constructor. Must define an explicit constructor."

This could be due to a mismatch between Java versions configured in eclipse and the ``pom.xml``.

1) Rightclick on project  
2) "Build Path"
3) "Configure Build Path..."
4) "Libraries"
5) "JRE System Library"
6) Set accordingly, or if not possible, change in ``pom.xml``
7) Clean afterwards: "Project" - "Clean"
   
